<?php

/**
 * Created by PhpStorm.
 * User: simony
 * Date: 2/08/2016
 * Time: 3:50 PM
 */
class Fontis_Australia_Model_Mysql4_Shipping_Carrier_Dropshipzone
    extends Mage_Core_Model_Mysql4_Abstract
{
    const DROPSHIPZONE_SUPPLIER_NAME = 'DropShipZone';

    protected function _construct()
    {
        $this->_init('australia/dropshipzone', 'sku');
    }

    public function getRate(Mage_Shipping_Model_Rate_Request $request)
    {
        $price = 0;
        $totalDropShipZoneQty = 0;
        $totalDropShipZoneWeight = 0;
        $state = $request->getDestRegionCode();

        foreach ($request->getAllItems() as $item) {
            if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                continue;
            }

            // if not dropship zone item, skip
            if (self::DROPSHIPZONE_SUPPLIER_NAME != $item->getProduct()->getSupplier()) {
                continue;
            }

            $qty = $item->getQty();
            $totalDropShipZoneQty += $qty;
            $totalDropShipZoneWeight += $item->getWeight() * $qty;

            $rateRow = $this->getRateRow($item->getSku());

            if (isset($rateRow[$state])) {
                $price += $qty * $rateRow[$state];
            }
        }

        $result = array(
            'price'     => $price,
            'qty'       => ceil($totalDropShipZoneQty),
            'weight'    => $totalDropShipZoneWeight
        );

        Mage::log(var_export($result, true));

        return $result;
    }

    public function getRateRow($sku)
    {
        $read = $this->_getReadAdapter();
        $table = $this->getMainTable();

        $select = $read->select()->from($table);
        $select->where('sku=?',$sku);

        $row = $read->fetchRow($select);

        return $row;
    }
}